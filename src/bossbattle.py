pillars = int(input())
if pillars > 2:
    print(pillars - 2)
else:
    print(1)

# Calculated solution
# cur = -1
# boss = pillars - 1
# cnt = 0
#
# # 4 = 2
# # 6 = 4
# # 7 = 5
# # 11 = 9
# # 14 = 12
# # 100 = 98
# while True:
#     cnt += 1
#     cur = (cur + 2) % pillars
#     if cur == boss or (cur + 1) % pillars == boss or (cur - 1 + pillars) % pillars == boss:
#         print(cnt)
#         exit(0)
#     boss = (boss + 1) % pillars


