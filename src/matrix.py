import sys


i = 1
while True:
    line = sys.stdin.readline()
    if not line:
        break
    a, b = map(int, line.split())
    c, d = map(int, sys.stdin.readline().split())

    det = a * d - b * c

    print(f'Case {i}:')
    print(d // det, -b // det)
    print(-c // det, a // det)
    i += 1
    sys.stdin.readline()
