import sys


d = {
    'T': 0,
    'C': 0,
    'G': 0
}
cards = [ch for ch in sys.stdin.readline().strip()]
for card in cards:
    d[card] += 1
score = 0
for count in d.values():
    score += count ** 2
score += min(d.values()) * 7
print(score)
