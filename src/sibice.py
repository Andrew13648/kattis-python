import sys
import math


arr = sys.stdin.readline().split()
n = int(arr[0])
h = int(arr[1])
w = int(arr[2])

for i in range(n):
    m = int(sys.stdin.readline())
    print("DA" if m <= math.sqrt(h * h + w * w) else "NE")

