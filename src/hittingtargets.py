import math


targets = int(input())
rects = []
circles = []

for _ in range(targets):
    arr = input().split()
    if arr[0] == "rectangle":
        rects.append([int(x) for x in arr[1:]])
    else:
        circles.append([int(x) for x in arr[1:]])

shots = int(input())
for _ in range(shots):
    x, y = map(int, input().split())
    hit = 0
    for circle in circles:
        if math.sqrt((x - circle[0]) ** 2 + (y - circle[1]) ** 2) <= circle[2]:
            hit += 1
    for rect in rects:
        if x >= rect[0] and x <= rect[2] and y >= rect[1] and y <= rect[3]:
            hit += 1

    print(hit)

