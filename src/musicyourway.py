import sys
import operator


attributes = sys.stdin.readline().split()
m = int(sys.stdin.readline())
songs = []
for _ in range(m):
    songs.append(sys.stdin.readline().split())

n = int(sys.stdin.readline())

for _ in range(n):
    sort_by = sys.stdin.readline().strip()
    songs.sort(key=operator.itemgetter(attributes.index(sort_by)))
    print(' '.join(attributes))
    for song in songs:
        print(' '.join(song))
    print()
