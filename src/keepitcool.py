import sys
from operator import itemgetter


bottles, students, _, capacity = map(int, sys.stdin.readline().split())
slots = [int(x) for x in sys.stdin.readline().split()]
total_cold_sodas = sum(slots)

for i in range(len(slots)):
    slots[i] = [i, 0, slots[i]]

slots.sort(key=itemgetter(2))

for slot in slots:
    add_to_slot = min(bottles, capacity - slot[2])
    slot[1] = add_to_slot
    bottles -= add_to_slot
    if bottles == 0:
        break

# Need to sum the number of spots where we didn't add a soda
total_not_added = sum(slot[2] for slot in slots if slot[1] == 0)

if total_not_added < students:
    print('impossible')
else:
    slots.sort(key=itemgetter(0))
    print(' '.join([str(slot[1]) for slot in slots]))
