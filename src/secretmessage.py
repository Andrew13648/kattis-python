import math
import sys


n = int(sys.stdin.readline())
for _ in range(n):
    s = sys.stdin.readline().strip()
    k = math.ceil(math.sqrt(len(s)))

    encrypted = []
    for i in range(k):
        for j in range(k):
            idx = k * (k - j - 1) + i
            if idx < len(s):
                encrypted.append(s[idx])
    print(''.join(encrypted))


