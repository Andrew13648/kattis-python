import sys


res = 0
p, t = map(int, sys.stdin.readline().split())
for _ in range(p):
    solved = True
    for _ in range(t):
        s = sys.stdin.readline()
        if s[1:] != s[1:].lower():
            solved = False
    if solved:
        res += 1
print(res)
