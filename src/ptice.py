import sys


guesses = ['ABC', 'BABC', 'CCAABB']
names = ['Adrian', 'Bruno', 'Goran']
correct = [0, 0, 0]
best = 0

sys.stdin.readline()
answers = sys.stdin.readline().strip()

for i, answer in enumerate(answers):
    for j, guess in enumerate(guesses):
        if guesses[j][i % len(guesses[j])] == answer:
            correct[j] += 1
            best = max(best, correct[j])

print(best)
for c, n in zip(correct, names):
    if c == best:
        print(n)
