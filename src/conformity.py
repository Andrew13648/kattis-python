import sys
from collections import defaultdict


combinations = defaultdict(int)
n = int(sys.stdin.readline())

for _ in range(n):
    courses = sys.stdin.readline().split()
    courses.sort()
    combinations[tuple(courses)] += 1

max_students = max(combinations.values())
students = 0
for x in combinations.values():
    if x == max_students:
        students += x
print(students)
