import sys


# Floating point solution
# def calc(arr, pos):
#     if pos + 1 == len(arr):
#         return arr[pos]
#     return arr[pos] + 1 / calc(arr, pos + 1)

def add(a, b, c, d):
    return a * d + c * b, b * d


def calc(arr, pos):
    if pos + 1 == len(arr):
        return arr[pos], 1
    val = calc(arr, pos + 1)
    return add(arr[pos], 1, val[1], val[0])


sys.stdin.readline().strip()
arr = [int(n) for n in sys.stdin.readline().strip().split()]


res = calc(arr, 0)
print(f"{res[0]}/{res[1]}")
