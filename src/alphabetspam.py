import sys


line = sys.stdin.readline().strip()
upper = lower = spaces = symbols = 0
for ch in line:
    if ch.isupper():
        upper += 1
    elif ch.islower():
        lower += 1
    elif ch == '_':
        spaces += 1
    else:
        symbols += 1
print(spaces / len(line))
print(lower / len(line))
print(upper / len(line))
print(symbols / len(line))
