import sys
from collections import defaultdict


def printb(b):
    print("true" if b else "false")


d = defaultdict(int)

for line in sys.stdin:
    arr = line.split()
    if arr[0] == "define":
        d[arr[2]] = int(arr[1])
    elif arr[1] not in d or arr[3] not in d:
        print("undefined")
    elif arr[2] == "=":
        printb(d[arr[1]] == d[arr[3]])
    elif arr[2] == ">":
        printb(d[arr[1]] > d[arr[3]])
    else:
        printb(d[arr[1]] < d[arr[3]])
