import sys


def right_shift(s):
    return s[-1] + s[0:-1]


s = sys.stdin.readline().strip()


for n in range(1, len(s)):
    # Only check valid substrings
    if len(s) % n == 0:
        for k in range(len(s) // n - 1):
            ss = s[k * n:(k + 1) * n]
            next_ss = s[(k + 1) * n:(k + 2) * n]
            if right_shift(ss) != next_ss:
                break
        else:
            print(n)
            exit(0)
print(len(s))

