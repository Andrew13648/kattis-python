import sys


case = 1
while True:
    ints = []
    s = sys.stdin.readline()
    if not s:
        break
    n = int(s)
    for _ in range(n):
        ints.append(int(sys.stdin.readline()))

    queries = []
    m = int(sys.stdin.readline())
    for _ in range(m):
        queries.append(int(sys.stdin.readline()))

    print(f'Case {case}:')
    for q in queries:
        closest = ints[0] + ints[1]
        for a in ints:
            for b in ints:
                if a != b:
                    if abs(a + b - q) < abs(closest - q):
                        closest = a + b
        print(f'Closest sum to {q} is {closest}.')
    case += 1
