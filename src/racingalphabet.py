import sys
import math


def pos(ch):
    if ch == ' ':
        return 0
    elif ch == '\'':
        return 1
    else:
        return ord(ch) - ord('A') + 2


DISTANCE_BETWEEN = math.pi * 60 / 28
n = int(sys.stdin.readline())
for _ in range(n):
    s = sys.stdin.readline().strip()
    cur_pos = pos(s[0])
    dist = 0

    for i in range(1, len(s)):
        next_pos = pos(s[i])
        a = (cur_pos - next_pos) % 28
        b = (next_pos - cur_pos) % 28

        dist += DISTANCE_BETWEEN * min(a, b)
        cur_pos = next_pos
    print(dist / 15 + len(s))
