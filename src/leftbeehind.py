import sys


for line in sys.stdin:
    line = line.split()
    sweet = int(line[0])
    sour = int(line[1])
    if sweet == 0 and sour == 0:
        break
    elif sweet + sour == 13:
        print("Never speak again.")
    elif sweet > sour:
        print("To the convention.")
    elif sour > sweet:
        print("Left beehind.")
    else:
        print("Undecided.")
