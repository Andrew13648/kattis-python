import sys


width = int(sys.stdin.readline())
n = int(sys.stdin.readline())
area = 0
for _ in range(n):
    arr = sys.stdin.readline().split()
    area += int(arr[0]) * int(arr[1])
print(area // width)
