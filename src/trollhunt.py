import sys


b, k, g = [int(x) for x in sys.stdin.readline().split()]

# 5 5 3 == 4
# 5 5 4 == 4
# 5 2 1 == 2
# 7 5 1 == 2
# 10 5 2 = 5

groups = k // g
days = (b + groups - 1) // groups

if b - groups * (days - 1) <= 1:
    print(days - 1)
else:
    print(days)

