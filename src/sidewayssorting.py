import sys


while True:
    r, c = map(int, sys.stdin.readline().split())
    if r == c == 0:
        break
    lines = []
    for _ in range(r):
        lines.append(sys.stdin.readline().strip())

    cols = []
    for i in range(c):
        col = ''
        for j in range(r):
            col += lines[j][i]
        cols.append(col)

    cols.sort(key=lambda x: x.lower())

    for i in range(r):
        for j in range(c):
            print(cols[j][i], end='')
        print()
    print()

