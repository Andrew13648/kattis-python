import sys


sys.stdin.readline()
lst = map(int, sys.stdin.readline().split())

d = {}
for i in range(0, 256):
    d[(i ^ (i << 1)) & 255] = i

print(' '.join([str(d[b]) for b in lst]))
