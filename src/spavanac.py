import sys


arr = sys.stdin.readline().split()
h = int(arr[0])
m = int(arr[1])

m = m - 45
if m < 0:
    m = 60 + m
    h = h - 1 if h > 0 else 23

print(f"{h} {m}")

