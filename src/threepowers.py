import sys


# Calculate powers of 3
powers = []
val = 1
for i in range(100):
    powers.append(val)
    val *= 3

while True:
    # Read the number and convert it to a binary representation
    n = int(sys.stdin.readline())
    if n == 0:
        break
    n -= 1
    s = f"{n:b}"
    arr = []

    # Now convert the binary representation to powers of three
    for i, ch in enumerate(reversed(s)):
        if ch == '1':
            arr.append(powers[i])
    print(f"{{ {', '.join([str(x) for x in arr])} }}")



