import sys


def minutes(s):
    lst = s.split(':')
    return int(lst[0]) * 60 + int(lst[1])


for line in sys.stdin:

    lst = line.split()
    sunrise_minutes = minutes(lst[3])
    sunset_minutes = minutes(lst[4])
    diff = sunset_minutes - sunrise_minutes
    print(lst[0], lst[1], lst[2], diff // 60, 'hours', diff % 60, 'minutes')
