import sys
from collections import defaultdict


lists = 0
while True:
    lists += 1
    n = int(sys.stdin.readline())
    animals = defaultdict(int)
    if n == 0:
        break
    for _ in range(n):
        animal = sys.stdin.readline().split()[-1].lower()
        animals[animal] += 1
    print(f'List {lists}:')
    for animal in sorted(animals.keys()):
        print(f'{animal} | {animals[animal]}')
