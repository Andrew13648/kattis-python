import sys


sys.stdin.readline()
all = {x for x in 'abcdefghijklmnopqrstuvwxyz'}
for phrase in sys.stdin:
    phrase = phrase.lower()
    diff = all - set(phrase)
    if len(diff) == 0:
        print('pangram')
    else:
        print('missing', ''.join(sorted(diff)))
