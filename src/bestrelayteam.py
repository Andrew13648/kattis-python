import sys


def calculate_time(a, b, c, d):
    return a[1] + b[2] + c[2] + d[2]


# First sort the runners by their flying start time
runners = []
n = int(sys.stdin.readline())
for _ in range(n):
    splt = sys.stdin.readline().split()
    runners.append((splt[0], float(splt[1]), float(splt[2])))
runners.sort(key=lambda x: x[2])

# Now determine the best team
time = 0
best = (runners[0], runners[1], runners[2], runners[3])
for i in range(len(runners)):
    team = [runners[i]]

    for j in range(4):
        if i != j:
            team.append(runners[j])
        if len(team) == 4:
            break
    if calculate_time(*team) < calculate_time(*best):
        best = team

print(calculate_time(*best))
for runner in best:
    print(runner[0])
