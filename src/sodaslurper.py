e, f, c = map(int, input().split())
empty_bottles = e + f
drank = 0
while empty_bottles >= c:
    drank += empty_bottles // c
    empty_bottles = empty_bottles // c + empty_bottles % c
print(drank)
