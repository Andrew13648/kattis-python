import sys


sys.stdin.readline()
items = [int(x) for x in sys.stdin.readline().split()]
items.sort(reverse=True)
discount = 0
for i in range(0, len(items), 3):
    if i + 2 < len(items):
        discount += items[i + 2]

print(discount)
