s = input()
k = input()
decrypted = ''

for i, c in enumerate(s):
    if i % 2 == 0:
        shift = (-(ord(k[i]) - ord('A')) + 26) % 26
    else:
        shift = ord(k[i]) - ord('A')

    decrypted += chr((ord(c) - ord('A') + shift) % 26 + ord('A'))

print(decrypted)
