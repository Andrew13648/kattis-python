import sys
import math


# See https://www.youtube.com/watch?time_continue=67&v=7iNwyqeEH6Y for an explanation
n = int(sys.stdin.readline())
if n > 100:
    print(1 - 1 / math.e)
    exit(0)

p = 0.0
for i in range(n):
    if i % 2 == 0:
        p += 1 / math.factorial(i + 1)
    else:
        p -= 1 / math.factorial(i + 1)

print(p)
