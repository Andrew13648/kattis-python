import sys


sys.stdin.readline()
lst = [int(x) for x in sys.stdin.readline().split()]
lst.sort(reverse=True)
alice = 0
bob = 0
for i, n in enumerate(lst):
    if i % 2 == 0:
        alice += n
    else:
        bob += n
print(alice, bob)
