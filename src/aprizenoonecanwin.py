import sys


arr = sys.stdin.readline().split()
min_cost = int(arr[1])
prices = [int(x) for x in sys.stdin.readline().split()]
prices.sort()

max_items = 1
for i in range(len(prices) - 1):
    if prices[i] + prices[i + 1] > min_cost:
        break
    else:
        max_items = max_items + 1

print(max_items)
