str = input()


rows = 1
columns = len(str)
for r in range(2, len(str)):
    c = len(str) // r
    if r * c == len(str) and r <= c:
        rows = r
        columns = c

arr = [""] * len(str)
for i in range(len(str)):
    ir = i // columns
    ic = i % columns
    arr[i] = str[ic * rows + ir] # col-major order
    # arr[i] = str[ir * columns + ic] # row-major order
print("".join(arr))
