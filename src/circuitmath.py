import sys


sys.stdin.readline()
bools = [True if x == 'T' else False for x in sys.stdin.readline().split()]
stack = []
for op in sys.stdin.readline().split():

    if op == '*':
        s1 = stack.pop()
        s2 = stack.pop()
        stack.append(s1 and s2)
    elif op == '+':
        s1 = stack.pop()
        s2 = stack.pop()
        stack.append(s1 or s2)
    elif op == '-':
        stack.append(not stack.pop())
    else:
        stack.append(bools[ord(op) - ord('A')])
print('T' if stack.pop() else 'F')
