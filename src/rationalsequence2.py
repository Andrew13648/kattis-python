import sys


def get_n(p, q):
    if p == 1 and q == 1:
        return 1
    elif p > q:
        return get_n(p - q, q) * 2 + 1
    else:
        return get_n(p, q - p) * 2


n = int(sys.stdin.readline())

for i in range(n):
    arr = sys.stdin.readline().split()
    p, q = map(int, arr[1].split("/"))
    print(i + 1, get_n(p, q))
