import sys


sys.stdin.readline()
notes = sys.stdin.readline().split()
g = ['' for _ in range(14)]
line_notes = {1, 3, 5, 7, 9, 13}

for note in notes:

    if note.isupper():
        index = 6 - (ord(note[0]) - ord('A'))
    else:
        index = 13 - (ord(note[0]) - ord('a'))

    if len(note) > 1:
        note_length = int(note[1:])
    else:
        note_length = 1

    for i in range(14):

        spacer = '-' if i in line_notes else ' '

        if i == index:
            g[i] += '*' * note_length + spacer
        else:
            g[i] += spacer * (note_length + 1)

note_names = ['G', 'F', 'E', 'D', 'C', 'B', 'A', 'g', 'f', 'e', 'd', 'c', 'b', 'a']
for i, x in enumerate(g):
    print(f'{note_names[i]}: {x[:-1]}')
