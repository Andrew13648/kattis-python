import sys
from operator import itemgetter


points = []
for line in sys.stdin:
    x, y = map(int, line.split())
    points.append((x, y))

points.sort(key=itemgetter(0))
missing_x = points[2][0] if points[0][0] == points[1][0] else points[0][0]
points.sort(key=itemgetter(1))
missing_y = points[2][1] if points[0][1] == points[1][1] else points[0][1]
print(missing_x, missing_y)
