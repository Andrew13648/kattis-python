import sys
from collections import defaultdict


n, m = map(int, sys.stdin.readline().split())
outcomes = defaultdict(int)
for i in range(1, n + 1):
    for j in range(1, m + 1):
        outcomes[i + j] += 1

sorted_outcomes = sorted(outcomes, key=outcomes.get, reverse=True)
best = outcomes[sorted_outcomes[0]]
for k in sorted_outcomes:
    if best == outcomes[k]:
        print(k)
    else:
        break
