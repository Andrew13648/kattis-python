a, b = map(int, input().split())


count = 0
for i in range(a, b):
    s = set()
    n = i
    while n > 0:
        d = n % 10
        if d == 0 or i % d != 0:
            break
        s.add(d)
        n //= 10
    else:
        if len(s) == 6:
            print(i)
            count += 1

print(count)

