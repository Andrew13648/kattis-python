import sys


while True:
    w, l = map(int, sys.stdin.readline().split())

    if w == 0 and l == 0:
        exit(0)

    n = int(sys.stdin.readline())

    thinks = [0, 0]
    actual = [0, 0]
    for _ in range(n):
        lst = sys.stdin.readline().split()
        dir, dist = lst[0], int(lst[1])
        if dir == 'u':
            thinks[1] += dist
            actual[1] = min(actual[1] + dist, l - 1)
        elif dir == 'd':
            thinks[1] -= dist
            actual[1] = max(actual[1] - dist, 0)
        elif dir == 'r':
            thinks[0] += dist
            actual[0] = min(actual[0] + dist, w - 1)
        else:
            thinks[0] -= dist
            actual[0] = max(actual[0] - dist, 0)

    print(f'Robot thinks {thinks[0]} {thinks[1]}')
    print(f'Actually at {actual[0]} {actual[1]}\n')

