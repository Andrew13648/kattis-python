import sys


w, p = map(int, sys.stdin.readline().split())
partitions = [0, *map(int, sys.stdin.readline().split()), w]

s = set()
for i in range(len(partitions)):
    for j in range(i + 1, len(partitions)):
        s.add(partitions[j] - partitions[i])

print(' '.join(map(str, sorted(s))))
