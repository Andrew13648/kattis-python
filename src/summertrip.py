import sys


events = sys.stdin.readline().strip()
good = 0
for i in range(len(events)):
    s = set()
    s.add(events[i])
    for j in range(i + 1, len(events)):
        if events[j] == events[i]:
            break
        elif events[j] in s:
            continue
        else:
            good += 1
            s.add(events[j])
print(good)
