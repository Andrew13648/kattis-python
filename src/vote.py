import sys


t = int(sys.stdin.readline())
for _ in range(t):
    candidates = int(sys.stdin.readline())
    votes = []
    for i in range(candidates):
        votes.append((i, int(sys.stdin.readline())))

    votes.sort(key=lambda x: x[1], reverse=True)

    total = sum(x[1] for x in votes)

    if votes[0][1] >= total // 2 + 1:
        print('majority winner', votes[0][0] + 1)
    elif len(votes) > 1 and votes[0][1] > votes[1][1]:
        print('minority winner', votes[0][0] + 1)
    else:
        print('no winner')
