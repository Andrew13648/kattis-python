import sys


_, length = map(int, sys.stdin.readline().split())

time = 0
pos = 0
for line in sys.stdin:
    d, r, g = map(int, line.split())
    time += d - pos
    pos = d

    if time % (r + g) < r:
        time += r - (time % (r + g))

time += length - pos
print(time)
