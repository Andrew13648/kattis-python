import sys


arr = sys.stdin.readline().split()
max_capacity = int(arr[1])
max_color_diff = int(arr[2])

colors = [int(n) for n in sys.stdin.readline().split()]
colors.sort()

machines = 1
current_color = colors[0]
current_fullness = 1

for i in range(1, len(colors)):
    if colors[i] - current_color > max_color_diff or current_fullness == max_capacity:
        current_color = colors[i]
        current_fullness = 1
        machines = machines + 1
    else:
        current_fullness = current_fullness + 1

print(machines)



