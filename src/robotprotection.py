import sys


# Calculate the area of a polygon using the Shoelace formula
# Points must be sorted in cw or ccw order
# See https://rechneronline.de/pi/simple-polygon.php
def polygon_area(vertices):
    n = len(vertices)
    area = 0.0
    for i in range(n):
        j = (i + 1) % n
        area += vertices[i][0] * vertices[j][1]
        area -= vertices[j][0] * vertices[i][1]
    return abs(area) / 2.0


def cross_product(p1, p2, p3):
    return (p2[0] - p1[0]) * (p3[1] - p1[1]) - (p2[1] - p1[1]) * (p3[0] - p1[0])


def slope(p1, p2):
    return (p1[1] - p2[1]) / (
                p1[0] - p2[0]) if p1[0] != p2[0] else float('inf')


def convex_hull(points):
    """
    Returns the convex hull of the given set of points using Graham scan.
    Note this does not include collinear points in the hull.
    """

    # Set the starting point as the bottom left most point
    start = min(points)
    points.pop(points.index(start))

    # Sort points ccw order
    points.sort(key=lambda p: (slope(p, start), -p[1], p[0]))

    # Determine the points in the convex hull by checking if each set of 3
    # points makes a ccw turn
    hull = [start]
    for p in points:
        hull.append(p)
        while len(hull) > 2 and cross_product(hull[-3], hull[-2], hull[-1]) < 0:
            hull.pop(-2)
    return hull


while True:
    beacons = int(sys.stdin.readline())
    if beacons == 0:
        break
    vertices = []
    for _ in range(beacons):
        vertices.append([int(x) for x in sys.stdin.readline().split()])

    hull = convex_hull(vertices)
    print(polygon_area(hull))
