import sys


p = int(sys.stdin.readline())
for k in range(1, p + 1):
    n = int(sys.stdin.readline().split()[1])
    print(k, n * (n + 1) // 2 + n)
