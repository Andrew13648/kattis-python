import sys


s = sys.stdin.readline().strip()
total = 0
for ch in s:
    if ch == 'W':
        total += 1
    else:
        total -= 1
if total == 0 and len(s) % 2 == 0:
    print(1)
else:
    print(0)
