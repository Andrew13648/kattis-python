import sys


s = sys.stdin.readline()

lst = list(s)
ans = []
for i in range(0, len(lst) - 1):
    if lst[i] == '<':
        del ans[len(ans) - 1]
    else:
        ans.append(lst[i])

print(''.join(ans))
