d = {}
while True:
    s = input()
    if s == "-1":
        break
    time, problem, result = s.split()
    if problem not in d:
        d[problem] = {}
        d[problem]["solved"] = False
        d[problem]["score"] = 0
    if d[problem]["solved"]:
        continue
    if result == "right":
        d[problem]["solved"] = True
        d[problem]["score"] += int(time)
    else:
        d[problem]["score"] += 20

score = 0
time_score = 0
for k, v in d.items():
    score += 1 if d[k]["solved"] else 0
    time_score += d[k]["score"] if d[k]["solved"] else 0
print(score, time_score)

