import sys


running = False
last = 0
total = 0
sys.stdin.readline()
for t in sys.stdin:
    cur = int(t)
    if running:
        total += cur - last
    last = cur
    running = not running

if running:
    print('still running')
else:
    print(total)
