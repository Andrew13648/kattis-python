import sys


proofs = {}
n = int(sys.stdin.readline())
for i in range(n):
    line = sys.stdin.readline().split()
    found_error = False
    if line[0] == '->':
        proofs[line[1]] = True
    else:
        for j in range(line.index('->')):
            if line[j] not in proofs:
                print(i + 1)
                found_error = True
                break
        else:
            proofs[line[-1]] = True
    if found_error:
        break
else:
    print('correct')
