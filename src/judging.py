import sys
from collections import defaultdict


domjudge_results = defaultdict(int)
kattis_results = defaultdict(int)
n = int(sys.stdin.readline())

for _ in range(n):
    domjudge_results[sys.stdin.readline()] += 1

for _ in range(n):
    kattis_results[sys.stdin.readline()] += 1

max_same = 0
for k in domjudge_results:
    max_same += min(domjudge_results[k], kattis_results[k])

print(max_same)
