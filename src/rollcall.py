import sys
from collections import defaultdict


first_name_counts = defaultdict(int)
names = []
for name in sys.stdin:
    first_name, last_name = name.split()
    names.append((last_name, first_name))
    first_name_counts[first_name] += 1

names.sort()

for name in names:
    if first_name_counts[name[1]] > 1:
        print(name[1], name[0])
    else:
        print(name[1])
