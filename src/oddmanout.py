import sys
from _collections import defaultdict


n = int(sys.stdin.readline())
for i in range(n):
    sys.stdin.readline()
    lst = [int(x) for x in sys.stdin.readline().split()]
    d = defaultdict(int)
    for code in lst:
        d[code] += 1
    for k, v in d.items():
        if v == 1:
            print(f'Case #{i + 1}: {k}')
