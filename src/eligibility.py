import sys


n = int(sys.stdin.readline())
for _ in range(n):
    name, began_studies, dob, courses = sys.stdin.readline().split()
    courses = int(courses)
    if int(began_studies[:4]) >= 2010 or \
       int(dob[:4]) >= 1991:
        print(name, 'eligible')
    elif courses > 40:
        print(name, 'ineligible')
    else:
        print(name, 'coach petitions')
