import sys


n = int(sys.stdin.readline())
for case in range(n):
    sys.stdin.readline()
    r = []
    b = []
    for segment in sys.stdin.readline().split():
        segment_length = int(segment[0:-1])
        if segment[-1] == 'R':
            r.append(segment_length)
        else:
            b.append(segment_length)
    r.sort(reverse=True)
    b.sort(reverse=True)
    length = 0
    for i in range(min(len(r), len(b))):
        length += r[i] + b[i] - 2
    print(f'Case #{case + 1}: {length}')
