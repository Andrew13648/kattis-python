import sys


tests = int(sys.stdin.readline())
for i in range(tests):
    mirror = ''
    rows, cols = map(int, sys.stdin.readline().split())
    print('Test', i + 1)
    for _ in range(rows):
        mirror = ''.join(reversed(sys.stdin.readline().strip())) + '\n' + mirror
    print(mirror, end='')
