c, r, n = map(int, input().split())

# Initialize 2-d list to hold whether each cell has been visited
visited = [[False for i in range(r)] for j in range(c)]

visit_next = []
for i in range(n):
    a, b = map(int, input().split())
    visit_next.append([b - 1, a - 1])

# Breadth-first search on table
visited_count = 0
days = 0
d = [[-1, 0], [1, 0], [0, -1], [0, 1]]
while visited_count != c * r:
    days += 1
    visit_next_2 = []
    for [x, y] in visit_next:
        if not visited[y][x]:
            visited[y][x] = True
            visited_count += 1
            for [dx, dy] in d:
                if x + dx >= 0 and x + dx < len(visited[y]) and y + dy >= 0 and y + dy < len(visited):
                    visit_next_2.append([x + dx, y + dy])
    visit_next = visit_next_2

print(days)
