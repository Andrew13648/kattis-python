import sys


friend_right = int(sys.stdin.readline())
a = sys.stdin.readline().strip()
b = sys.stdin.readline().strip()
matching = 0
for c, d in zip(a, b):
    if c == d:
        matching += 1

if friend_right >= matching:
    ans = len(a) - friend_right + matching
else:
    ans = len(a) - matching + friend_right
print(ans)
