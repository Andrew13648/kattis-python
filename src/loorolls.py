import sys


length, n = map(int, sys.stdin.readline().split())

rolls = 1
total = 0
cur = n
while total < n:
    leftover = length % cur
    if leftover == 0:
        break
    total += leftover
    cur = cur - leftover
    rolls += 1

print(rolls)
