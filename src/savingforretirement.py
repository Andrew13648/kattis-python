import sys


b, b_r, b_s, a, a_s = map(int, sys.stdin.readline().split())
b_saved = (b_r - b) * b_s
a_saved = 0
while a_saved <= b_saved:
    a_saved += a_s
    a += 1
print(a);
