import sys


a = sys.stdin.readline().count('S')
b = sys.stdin.readline().count('S')
result = a * b

if result == 0:
    print(0)
else:
    print('S(' * result + '0' + ')' * result)
