import sys


n = int(sys.stdin.readline())
keywords = set()
for _ in range(n):
    keyword = sys.stdin.readline().strip()
    keyword = keyword.replace('-', ' ').lower()
    keywords.add(keyword)

print(len(keywords))
