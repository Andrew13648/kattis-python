import sys
import math


# This uses Brahmagupta's Formula to find the area
a, b, c, d = map(int, sys.stdin.readline().split())
s = (a + b + c + d) / 2
print(math.sqrt((s - a) * (s - b) * (s - c) * (s - d)))
