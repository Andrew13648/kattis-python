import sys


while True:
    n = int(sys.stdin.readline())
    if n == 0:
        break
    arr = []
    for i in range(n):
        arr.append([int(n) for n in sys.stdin.readline().split()])
    stan_score = 0
    ollie_score = 0
    brownie = arr[len(arr) // 2]
    for point in arr:
        if point[0] == brownie[0] or point[1] == brownie[1]:
            continue
        elif (point[0] > brownie[0] and point[1] > brownie[1]) or \
                (point[0] < brownie[0] and point[1] < brownie[1]):
            stan_score += 1
        else:
            ollie_score += 1
    print(stan_score, ollie_score)
