import sys


n, m = map(int, sys.stdin.readline().split())
key = sys.stdin.readline().strip()
ciphertext = sys.stdin.readline().strip()

# plaintext =     marywasnosyagain
# key =                        ...again
# cipher text =   pirpumsemoystoal
# secret prefix = diary
# key =           diarymarywasnosyagain
# The basic idea here is to build up what the key should be based on the portion of known plaintext
for i in range(m):
    key = [chr((ord(ciphertext[m - i - 1]) - ord(key[len(key) - i - 1])) % 26 + 97), *key]

print(''.join(key[n:]))
