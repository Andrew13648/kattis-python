import sys
import re


special_chars = '!"#$%&\'()*[\\\\\]'

for levels in sys.stdin:
    levels = int(levels)
    replacement = '\\' * (2 ** levels - 1)
    line = sys.stdin.readline().strip()

    s = f'{replacement * 2}\\1'
    print(re.sub(f'([{special_chars}])', f'{replacement * 2}\\1', line))
