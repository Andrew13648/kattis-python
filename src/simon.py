import sys


n = int(sys.stdin.readline())
for _ in range(n):
    lst = sys.stdin.readline().split()
    if len(lst) > 1 and lst[0] == 'simon' and lst[1] == 'says':
        print(' '.join(lst[2:]))
    else:
        print()
