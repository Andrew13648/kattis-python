import sys


max_diff = int(sys.stdin.readline())
arr = [ch for ch in sys.stdin.readline()]

current = 0
count = 0

while len(arr) > 0:
    if arr[0] == 'M' and current < max_diff:
        current += 1
        count += 1
        del arr[0]
    elif arr[0] == 'W' and current > -max_diff:
        current -= 1
        count += 1
        del arr[0]
    elif len(arr) > 1 and arr[1] == 'M' and current < max_diff:
        current += 1
        count += 1
        del arr[1]
    elif len(arr) > 1 and arr[1] == 'W' and current > -max_diff:
        current -= 1
        count += 1
        del arr[1]
    else:
        break

print(count)
