import sys
from collections import Counter

c = Counter(sys.stdin)

lst = list(c)
lst.sort()
total_trees = sum(c.values())
for species in lst:
    print(species.strip(), c[species] / total_trees * 100)

