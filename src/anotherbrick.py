import sys


h, w, n = map(int, sys.stdin.readline().split())
bricks = list(map(int, sys.stdin.readline().split()))

cur = 0
for _ in range(h):
    size = w
    while size > 0 and cur < len(bricks):
        size -= bricks[cur]
        cur += 1
    if size != 0:
        print('NO')
        break
else:
    print('YES')
