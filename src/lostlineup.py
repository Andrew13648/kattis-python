import sys
from operator import itemgetter


n = int(sys.stdin.readline())
people = map(int, sys.stdin.readline().split())

lst = []
for idx, ahead in enumerate(people):
    lst.append((idx + 2, ahead))
lst.sort(key=itemgetter(1))

res = [1]
for person in lst:
    res.append(person[0])
print(' '.join([str(x) for x in res]))
