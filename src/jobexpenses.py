import sys


sys.stdin.readline()
lst = map(int, sys.stdin.readline().split())
expenses = 0
for n in lst:
    if n < 0:
        expenses += abs(n)
print(expenses)
