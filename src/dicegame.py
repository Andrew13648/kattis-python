import sys


gunnar = [float(x) for x in sys.stdin.readline().split()]
emma = [float(x) for x in sys.stdin.readline().split()]

gunnar_sum = sum(gunnar)
emma_sum = sum(emma)

if (gunnar_sum > emma_sum):
    print("Gunnar")
elif (emma_sum > gunnar_sum):
    print("Emma")
else:
    print("Tie")
