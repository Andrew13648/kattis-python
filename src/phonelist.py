import sys


t = int(sys.stdin.readline())
for _ in range(t):
    numbers = []
    n = int(sys.stdin.readline())
    for _ in range(n):
        s = sys.stdin.readline().strip()
        numbers.append(s)

    numbers.sort()
    for i in range(len(numbers) - 1):

        if numbers[i + 1].startswith(numbers[i]):
            print('NO')
            break
    else:
        print('YES')
