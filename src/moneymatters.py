import sys


def root(lst, pos):
    while pos != lst[pos]:
        lst[pos] = lst[lst[pos]]
        pos = lst[pos]
    return pos


n, m = map(int, sys.stdin.readline().split())

friends = [x for x in range(n)]

amounts = []
for _ in range(n):
    amounts.append(int(sys.stdin.readline()))

friendships = []
for _ in range(m):
    friendships.append(tuple(map(int, sys.stdin.readline().split())))

for friendship in friendships:
    # union the friends into distinct groupings
    friends[root(friends, friendship[0])] = root(friends, friendship[1])

# Now find the sums for each group of friends
sums = [0 for _ in range(n)]
for i in range(n):
    if i != friends[i]:
        amounts[root(friends, i)] += amounts[i]
        amounts[i] = 0

for amount in amounts:
    if amount != 0:
        print("IMPOSSIBLE")
        break
else:
    print("POSSIBLE")
