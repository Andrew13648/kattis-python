import sys


s = sys.stdin.readline()
a = s[0:int(len(s) / 2)]
b = s[int(len(s) / 2):]

sum1 = 0
sum2 = 0
for ch1, ch2 in zip(a, b):
    sum1 += ord(ch1) - ord('A')
    sum2 += ord(ch2) - ord('A')

message = ''

for ch1, ch2 in zip(a, b):
    message += chr((ord(ch1) + sum1 + sum2 + ord(ch2)) % 26 + ord('A'))
print(message)
