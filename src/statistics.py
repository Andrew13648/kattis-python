import sys


for i, s in enumerate(sys.stdin):
    lst = list(map(int, s.split()[1:]))
    print(f'Case {i + 1}: {min(lst)} {max(lst)} {max(lst) - min(lst)}')
