import sys


n, q = map(int, sys.stdin.readline().split())

lst = []
for i in range(n):
    lst.append(i)


def join(a, b):
    ar = root(a)
    br = root(b)
    lst[ar] = br


def root(a):
    while a != lst[a]:
        lst[a] = lst[lst[a]]
        a = lst[a]
    return a


def check(a, b):
    return root(a) == root(b)


for _ in range(q):
    line = sys.stdin.readline().split()
    op = line[0]
    a = int(line[1])
    b = int(line[2])
    if op == '?':
        print('yes' if check(a, b) else 'no')
    else:
        join(a, b)
