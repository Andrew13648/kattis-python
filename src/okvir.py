import sys


m, n = map(int, sys.stdin.readline().split())
u, l, r, d = map(int, sys.stdin.readline().split())
puzzle = []
for line in sys.stdin:
    puzzle.append(line.strip())

rows = m + u + d
cols = n + l + r

for i in range(rows):
    for j in range(cols):
        if i < u or i >= u + m or j < l or j >= l + n:
            if i % 2 == j % 2:
                print('#', end='')
            else:
                print('.', end='')
        else:
            print(puzzle[i - u][j - l], end='')
    print()
