import sys
from zipfile import ZipFile


def extract_zip(input_zip):
    input_zip = ZipFile(input_zip)
    return {name: input_zip.read(name) for name in input_zip.namelist()}


print(extract_zip("in/aprizenoonecanwin.zip"))
if len(sys.argv) != 2:
    print("Specify the Kattis problem name as an argument.")
    exit(1)

print(f"Received {sys.argv[1]}")
